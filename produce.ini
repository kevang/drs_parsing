[]
prelude =
	tok_model = {
		'en': 'models/tok/eng.model',
		'de': 'models/tok/deu.model',
		'it': 'models/tok/ita.model',
		'nl': 'models/tok/nld.model',
	}
	udpipe_model = {
		'de': 'models/udpipe/german-gsd-ud-2.5-191206.udpipe',
		'it': 'models/udpipe/italian-isdt-ud-2.5-191206.udpipe',
		'nl': 'models/udpipe/dutch-alpino-ud-2.5-191206.udpipe',
	}

### LEMMA SUBSTITUTION ########################################################

# We cheat in the same way as Fancellu et al. (2019): we create our own version
# of the dev/test data where WordNet concepts have been replaced by lemmas for
# the respective language ("translated").

[out/pmb-2.2.0.%{lang}.%{portion}.%{metal}.tok]
cond = %{lang in ('de', 'it', 'nl') and portion in ('train', 'dev', 'test') and metal in ('gold', 'silver')}
dep.clf2tok = system/clf2tok.py
dep.clf = ext/pmb-2.2.0/exp_data/%{lang}/%{metal}/%{portion}.txt
recipe =
	set -e
	set -o pipefail
	cat %{clf} | python3 %{clf2tok} > %{target}

[out/pmb-2.2.0.%{lang}.%{portion}.%{metal}.lemma]
cond = %{lang in ('de', 'it', 'nl') and portion in ('train', 'dev', 'test') and metal in ('gold', 'silver')}
dep.tok = out/pmb-2.2.0.%{lang}.%{portion}.%{metal}.tok
dep.model = %{udpipe_model[lang]}
dep.udpipe = ext/udpipe-1.2.0-bin/bin-linux64/udpipe
recipe =
	set -e
	set -o pipefail
	cat %{tok} | %{udpipe} --tag --input=horizontal %{model} | egrep -v '^#' | cut -f 3 | egrep -v '^ø$' > %{target}

[out/pmb-2.2.0.%{lang}.%{portion}.%{metal}.translated.clf]
cond = %{lang in ('de', 'it', 'nl') and portion in ('train', 'dev', 'test') and metal in ('gold', 'silver')}
dep.txt = ext/pmb-2.2.0/exp_data/%{lang}/%{metal}/%{portion}.txt
dep.lemma = out/pmb-2.2.0.%{lang}.%{portion}.%{metal}.lemma
dep.translate = system/translate.py
deps = system/vertical.py
recipe = python3 %{translate} %{txt} %{lemma} > %{target}

### ORACLES ###################################################################

[out/%{corpus}.en.train.%{metal}.oracles.jsonl]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0') and metal in ('gold', 'silver')}
dep.oracles = system/oracles.py
dep.clf = data/%{corpus}/%{metal}/train.txt
deps = system/clf.py system/prep.py system/transit.py system/finish.py
recipe =
	set -e
	set -o pipefail
	mkdir -p out
	python3 %{oracles} en %{clf} > %{target}

[out/pmb-2.2.0.%{lang}.train.silver.oracles.jsonl]
cond = %{lang in ('de', 'it', 'nl')}
dep.oracles = system/oracles.py
dep.clf = ext/pmb-2.2.0/exp_data/%{lang}/silver/train.txt
dep.lemma = out/pmb-2.2.0.%{lang}.train.silver.lemma
deps = system/clf.py system/prep.py system/transit.py system/finish.py
recipe =
	set -e
	set -o pipefail
	mkdir -p out
	python3 %{oracles} %{lang} %{clf} %{lemma} > %{target}

### RECONSTRUCTION ############################################################

# Reconstruct the training data from the oracles as a sanity check

[out/%{corpus}.en.train.%{metal}.reconstructed.clf]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0') and metal in ('gold', 'silver')}
dep.reconstruct = system/reconstruct.py
dep.oracles = out/%{corpus}.en.train.%{metal}.oracles.jsonl
deps = system/clf.py system/finish.py system/transit.py system/util.py
recipe =
	set -e
	set -o pipefail
	cat %{oracles} | python3 %{reconstruct} > %{target}

[out/pmb-2.2.0.%{lang}.train.silver.reconstructed.clf]
cond = %{lang in ('de', 'it', 'nl')}
dep.reconstruct = system/reconstruct.py
dep.oracles = out/pmb-2.2.0.%{lang}.train.silver.oracles.jsonl
deps = system/clf.py system/finish.py system/transit.py system/util.py
recipe =
	set -e
	set -o pipefail
	cat %{oracles} | python3 %{reconstruct} > %{target}

### TRAINING ##################################################################

[out/%{corpus}.en.train.sg0.model]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0')}
dep.init_model = system/init_model.py
dep.oracles = out/%{corpus}.en.train.gold.oracles.jsonl
recipe = python3 %{init_model} en %{oracles} %{target}

[out/pmb-2.2.0.%{lang}.train.s0.model]
cond = %{lang in ('de', 'it', 'nl')}
dep.init_model = system/init_model.py
dep.oracles = out/pmb-2.2.0.%{lang}.train.silver.oracles.jsonl
recipe = python3 %{init_model} %{lang} %{oracles} %{target}

[out/%{corpus}.en.train.sg%{i}.model]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0') and i.isdigit()}
dep.train = system/train.py
dep.silver_oracles = out/%{corpus}.en.train.silver.oracles.jsonl
dep.gold_oracles = out/%{corpus}.en.train.gold.oracles.jsonl
dep.prev_model = out/%{corpus}.en.train.sg%{int(i) - 1}.model
deps = system/parser.py system/elmo.py system/guess.py system/hyper.py system/rolesets.py system/clf.py system/transit.py system/vocab.py system/util.py
recipe =
	set -e
	set -o pipefail
	python3 %{train} en %{gold_oracles} %{prev_model} %{int(i)} %{target} %{silver_oracles} %{gold_oracles}

[out/pmb-2.2.0.%{lang}.train.s%{i}.model]
cond = %{lang in ('de', 'it', 'nl') and i.isdigit()}
dep.train = system/train.py
dep.silver_oracles = out/pmb-2.2.0.%{lang}.train.silver.oracles.jsonl
dep.prev_model = out/pmb-2.2.0.%{lang}.train.s%{int(i) - 1}.model
deps = system/parser.py system/elmo.py system/guess.py system/hyper.py system/rolesets.py system/clf.py system/transit.py system/vocab.py system/util.py
recipe =
	set -e
	set -o pipefail
	python3 %{train} %{lang} %{silver_oracles} %{prev_model} %{int(i)} %{target} %{silver_oracles}

[out/%{corpus}.en.train.sg%{i}g1.model]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0') and i.isdigit()}
dep.train = system/train.py
dep.gold_oracles = out/%{corpus}.en.train.gold.oracles.jsonl
dep.prev_model = out/%{corpus}.en.train.sg%{i}.model
deps = system/parser.py system/elmo.py system/guess.py system/hyper.py system/rolesets.py system/clf.py system/transit.py system/vocab.py system/util.py
recipe =
	set -e
	set -o pipefail
	python3 %{train} en %{gold_oracles} %{prev_model} 1 %{target} %{gold_oracles}

[out/%{corpus}.en.train.sg%{i}g%{j}.model]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0') and i.isdigit() and j.isdigit()}
dep.train = system/train.py
dep.gold_oracles = out/%{corpus}.en.train.gold.oracles.jsonl
dep.prev_model = out/%{corpus}.en.train.sg%{i}g%{int(j) - 1}.model
deps = system/parser.py system/elmo.py system/guess.py system/hyper.py system/rolesets.py system/clf.py system/transit.py system/vocab.py system/util.py
recipe =
	set -e
	set -o pipefail
	python3 %{train} en %{gold_oracles} %{prev_model} %{int(j)} %{target} %{gold_oracles}

### TESTING ###################################################################

[out/%{corpus}.en.%{portion}.tok]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0') and portion in ('dev', 'test')}
dep.raw = data/%{corpus}/gold/%{portion}.txt.raw
dep.txt2iob = system/txt2iob.py
dep.elephant = ext/elephant/src/elephant
dep.model = models/tok/eng.model
dep.iob2tok = system/iob2tok.py
recipe =
	set -e
	set -o pipefail
	cat %{raw} | python3 %{txt2iob} | PATH=ext/elephant/ext:$PATH python2 %{elephant} -m %{model} -F iob -f iob | python3 %{iob2tok} > %{target}

[out/competition_input.tok]
dep.raw = competition_input.txt
dep.txt2iob = system/txt2iob.py
dep.elephant = ext/elephant/src/elephant
dep.model = models/tok/eng.model
dep.iob2tok = system/iob2tok.py
recipe =
	set -e
	set -o pipefail
	cat %{raw} | python3 %{txt2iob} | PATH=ext/elephant/ext:$PATH python2 %{elephant} -m %{model} -F iob -f iob | python3 %{iob2tok} > %{target}

[out/%{corpus}.en.%{portion}.gold.sg%{i}g%{j}.clf]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0') and portion in ('dev', 'test') and i.isdigit() and j.isdigit()}
dep.decode = system/decode.py
deps = system/hyper.py system/vocab.py system/parser.py system/elmo.py system/guess.py system/prep.py system/rolesets.py system/clf.py system/transit.py system/finish.py system/util.py system/horizontal.py system/vertical.py
dep.tok = out/%{corpus}.en.%{portion}.tok
dep.oracles = out/%{corpus}.en.train.gold.oracles.jsonl
dep.model = out/%{corpus}.en.train.sg%{i}g%{j}.model
recipe =
	set -e
	set -o pipefail
	python3 %{decode} en %{oracles} %{model} %{tok} > %{target}

[out/pmb-2.2.0.%{lang}.%{portion}.gold.s%{i}.clf]
cond = %{lang in ('de', 'it', 'nl') and portion in ('dev', 'test') and i.isdigit()}
dep.decode = system/decode.py
deps = system/hyper.py system/vocab.py system/parser.py system/elmo.py system/guess.py system/prep.py system/rolesets.py system/clf.py system/transit.py system/finish.py system/util.py system/horizontal.py system/vertical.py
dep.tok = out/pmb-2.2.0.%{lang}.%{portion}.gold.tok
# Cheating!
dep.lemmas = out/pmb-2.2.0.%{lang}.%{portion}.gold.lemma
dep.oracles = out/pmb-2.2.0.%{lang}.train.silver.oracles.jsonl
dep.model = out/pmb-2.2.0.%{lang}.train.s%{i}.model
recipe =
	set -e
	set -o pipefail
	python3 %{decode} %{lang} %{oracles} %{model} %{tok} %{lemmas} > %{target}

### EVALUATION ################################################################

[out/%{corpus}.en.train.gold.reconstructed.eval]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0')}
dep.pred = out/%{corpus}.en.train.gold.reconstructed.clf
dep.gold = data/%{corpus}/gold/train.txt
dep.counter = evaluation/counter.py
extra_options =
recipe =
	set -e
	set -o pipefail
	python3 %{counter} -f1 %{pred} -f2 %{gold} -prin -s conc -r 20 -ill dummy %{extra_options} > %{target}

[out/pmb-2.2.0.%{lang}.train.silver.reconstructed.eval]
cond = %{lang in ('de', 'it', 'nl')}
dep.pred = out/pmb-2.2.0.%{lang}.train.silver.reconstructed.clf
dep.gold = ext/pmb-2.2.0/exp_data/%{lang}/silver/train.txt
dep.counter = evaluation/counter.py
extra_options =
recipe =
	set -e
	set -o pipefail
	python3 %{counter} -f1 %{pred} -f2 %{gold} -prin -s conc -r 20 -ill dummy %{extra_options} > %{target}

[out/%{corpus}.en.%{portion}.gold.sg%{i}g%{j}.eval]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0') and portion in ('dev', 'test') and i.isdigit() and j.isdigit()}
dep.gold = data/%{corpus}/gold/%{portion}.txt
dep.pred = out/%{corpus}.en.%{portion}.gold.sg%{i}g%{j}.clf
dep.counter = evaluation/counter.py
extra_options =
recipe =
	set -e
	set -o pipefail
	python3 %{counter} -f1 %{pred} -f2 %{gold} -prin -s conc -r 20 -ill dummy %{extra_options} > %{target}

[out/pmb-2.2.0.%{lang}.%{portion}.gold.s%{i}.eval]
cond = %{lang in ('de', 'it', 'nl') and portion in ('dev', 'test') and i.isdigit()}
dep.gold = out/pmb-2.2.0.%{lang}.%{portion}.gold.translated.clf
dep.pred = out/pmb-2.2.0.%{lang}.%{portion}.gold.s%{i}.clf
dep.counter = evaluation/counter.py
extra_options =
recipe =
	set -e
	set -o pipefail
	python3 %{counter} -f1 %{pred} -f2 %{gold} -prin -s conc -r 20 -ill dummy %{extra_options} > %{target}

[out/%{corpus}.en.%{portion}.gold.sg%{i}g%{j}.structeval]
cond = %{corpus in ('pmb-2.1.0', 'pmb-2.2.0') and portion in ('dev', 'test') and i.isdigit() and j.isdigit()}
dep.gold = data/%{corpus}/gold/%{portion}.txt
dep.pred = out/%{corpus}.en.%{portion}.gold.sg%{i}g%{j}.clf
dep.counter = evaluation/counter.py
extra_options = -dse -dr -dc
recipe =
	set -e
	set -o pipefail
	python3 %{counter} -f1 %{pred} -f2 %{gold} -prin -s conc -r 20 -ill dummy %{extra_options} > %{target}

[out/pmb-2.2.0.%{lang}.%{portion}.gold.s%{i}.structeval]
cond = %{lang in ('de', 'it', 'nl') and portion in ('dev', 'test') and i.isdigit()}
dep.gold = out/pmb-2.2.0.%{lang}.%{portion}.gold.clf
dep.pred = out/pmb-2.2.0.%{lang}.%{portion}.gold.sg%{i}g%{j}.clf
dep.counter = evaluation/counter.py
extra_options = -dse -dr -dc
recipe =
	set -e
	set -o pipefail
	python3 %{counter} -f1 %{pred} -f2 %{gold} -prin -s conc -r 20 -ill dummy %{extra_options} > %{target}

### COMPETITION ###############################################################

[out/clfs.txt]
dep.decode = system/decode.py
deps = system/hyper.py system/vocab.py system/parser.py system/elmo.py system/guess.py system/prep.py system/rolesets.py system/clf.py system/transit.py system/finish.py system/util.py system/horizontal.py system/vertical.py
dep.tok = out/competition_input.tok
dep.oracles = out/pmb-2.2.0.en.train.gold.oracles.jsonl
# TODO choose better model for next competition
dep.model = out/pmb-2.2.0.en.train.sg0g20.model
recipe =
	set -e
	set -o pipefail
	python3 %{decode} en %{oracles} %{model} %{tok} > %{target}

[out/competition_output.zip]
dep.clf = out/clfs.txt
recipe =
	set -e
	set -o pipefail
	cd out
	zip competition_output.zip clfs.txt

#!/bin/bash


set -e
set -o pipefail
produce out/train.gold.model
produce out/dev.gold.{1..20}.eval
cat out/dev.gold.{1..20}.eval | egrep '^F-score' | nl

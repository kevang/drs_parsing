import numpy as np


from allennlp.commands.elmo import ElmoEmbedder
from elmoformanylangs import Embedder


elmo_en = None
elmo_de = None
elmo_nl = None
elmo_it = None


def embed_sentence(sentence, lang='en'):
    global elmo_en, elmo_de, elmo_it, elmo_nl
    if lang == 'en':
        if not elmo_en:
            elmo_en = ElmoEmbedder(options_file='models/elmo/elmo_2x4096_512_2048cnn_2xhighway_options.json', weight_file='models/elmo/elmo_2x4096_512_2048cnn_2xhighway_weights.hdf5')
        return np.average(elmo_en.embed_sentence(sentence), axis=0)
    elif lang == 'de':
        if not elmo_de:
            elmo_de = Embedder('models/ElMoForManyLangs/de')
        return elmo_de.sents2elmo((sentence,))[0]
    elif lang == 'it':
        if not elmo_de:
            elmo_de = Embedder('models/ElMoForManyLangs/it')
        return elmo_de.sents2elmo((sentence,))[0]
    elif lang == 'nl':
        if not elmo_de:
            elmo_de = Embedder('models/ElMoForManyLangs/nl')
        return elmo_de.sents2elmo((sentence,))[0]
    else:
        raise ValueError('language not supported: {}'.format(lang))
